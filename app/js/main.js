$(function () {

    $('[data-toggle="tooltip"]').tooltip();

    $(".show-menu").on('click', function(){
        $(".sidebar").addClass('open');
    });

    $(".sidebar-overlay").on('click', function(){
        $(".sidebar").removeClass('open');
    });

    $('.datepicker').datepicker();


    /*
    *   ACCOUNT DROPDOWN
    * */
    $(".account-dropdown button").on('click', function(){
        $(this).parent().toggleClass('open');
    });
    $(document).click(function (e) {
        e.stopPropagation();
        var container = $(".account-dropdown");
        if (container.has(e.target).length === 0) {
            $(".account-dropdown").removeClass('open');
        }
    });


});
