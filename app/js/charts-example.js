$(function () {



    function getjustHoursOfDay() {
        var hours=[];
        for (var i = 0; i < 12; i++) {
            var _time;
            if (i<10) {
                _time="0"+i+":00";
            }else {
                _time=i+":00";
            }
            hours.push(_time);
        }
        return hours;
    }
    var hourOfDay;
    hourOfDay = getjustHoursOfDay();

    var chartData = {
        labels: hourOfDay,
        datasets: [
            {
                type: "line",
                label: "Yellow data line",
                borderColor: '#E7BB25',
                borderWidth: 1,
                fill: false,
                data: [0, 19, 3, 5, 2, 13, 4, -10, -3, 5, 2, 13, 18],
            },
            {
                type: "line",
                label: "Blue data line",
                fill: false,
                data: [0, -4, -10, 4, 2, 21, 0, -4, 10, -4, 2, 21, 12],
                borderColor: '#2B9BDA',
                borderWidth: 1
            }
        ]
    };


    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myMixedChart = new Chart(ctx, {
            type: "bar",
            data: chartData,
            options: {
                responsive: true,
                legend: {
                    display: false
                },
                tooltips: {
                    mode: "index",
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true
                        },
                        gridLines: {
                            color: "#4A3F3D"
                        }
                    }],
                    yAxes: [{
                        display: false
                    }]
                },
                annotation: {
                    events: ["click"],
                    annotations: [
                        {
                            id: "hline",
                            type: "line",
                            mode: "horizontal",
                            scaleID: "y-axis-0",
                            value: 0,
                            borderColor: "#4A3F3D",
                            borderWidth: 1
                        }
                    ]
                },
                tooltips: {
                    backgroundColor: '#342F3B',
                    yAlign: 'bottom',
                    xAlign: 'center',
                    tooltipCornerRadius: 0,
                    tooltipXPadding: 10,
                    tooltipYPadding: 10
                }
            }
        });
    };






});