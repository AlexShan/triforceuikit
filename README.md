### install:
```sh
npm install --save-dev
```

Using Bootstrap 4.0.0-beta.3, downloaded and building localy without node_modules. 
The Bootstrap dist located in ./scss/bootstrap and including in _bootstrap.scss

#### Used dependencies
```sh
"browser-sync": "^2.18.8",
"gulp": "^3.9.1",
"gulp-concat": "^2.6.1",
"gulp-concat-js": "^0.1.0",
"gulp-file-include": "^1.0.0",
"gulp-rename": "^1.2.2",
"gulp-sass": "^3.1.0",
"gulp-sourcemaps": "^2.4.1",
"gulp-uglify": "^2.1.0"
```
